#!/bin/bash

# Bu plugin Monites Monitoring uygulamasýihazlardaki lisans ve bakýihlerini kontrol edilerek bildirim yapýnýar.
# Her hakkýý
# Yazan: Bayram Karagoz [bayram@monites.com]

NOW=$(date +"%s")
EXPIRE=$(date -d"$1" +%s)

DIFFSEC=`expr ${EXPIRE} - ${NOW}`

DAYS=$((${DIFFSEC}/60/60/24))

if [ $DAYS -gt $2 ]
then
           echo "OK - Bakim zamaninin dolmasina $DAYS gun var|Gun=$DAYS;$2;$3"
           exit 0
elif [[ $DAYS -gt $3 && $DAYS -lt $2 ]]
then
           echo "UYARI - Bakim zamaninin dolmasina $DAYS gun kaldi! $2 UYARI Seviyesi Asildi|Gun=$DAYS;$2;$3"
           exit 1
elif [ $DAYS -le $3 ]
then
           echo "KRITIK - Bakim zamaninin dolmasina $DAYS gun kaldi! $3 Kritik Seviyesi Asildi|Gun=$DAYS;$2;$3"
           exit 2
else
        echo "Bilinmeyen Durum"
        exit 3
fi
#!/bin/bash

sipp -s $1 $2 -sf /opt/sipp/uac.xml -inf /opt/sipp/userinfo.txt -m 1 &> /dev/null
if [ "$?" == "0" ];
 then
 echo "$2 SIP Serveri uzerinden $1 Arama Testi Basarili"
 exit 0
 else
 echo "$2 SIP Serveri uzerinden $1 Arama Testi Basarisiz!"
 exit 2
 fi

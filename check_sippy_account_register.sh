#!/usr/local/bin/bash

# Bu plugin Monites Monitoring uygulamasının Sippy Softswitch te 
# belirli bir kullanıcının register durumunun database sorgusu ile belirlenebilmesi amacıyla yazılmıştır.
# Her hakkı saklıdır.
# Yazan: Bayram Karagoz [bayram@monites.com]

REGISTER=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "select count(1) from location where username = '$1'")
DETAIL=$(/usr/local/bin/psql -U sippy -d sippy -c "select username, received, user_agent from location where username = '$1'")

if [ $REGISTER -ge 1 ]
then
               echo "OK - **$1 kullanicisi register durumda**"
               echo "$DETAIL"
               exit 0
else
              echo "KRITIK - **$1 kullanicisi register degil**"
              exit 2
fi
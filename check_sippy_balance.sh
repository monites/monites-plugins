#!/usr/local/bin/bash

# Yazan: Bayram Karagoz [bayram@monites.com]



ACCOUNT=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "select count(1) from accounts where username = '$1' and credit_limit + balance < $2")

CUSTNAME=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "SELECT username from accounts where username = '$1'")
CREDIT=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "SELECT credit_limit from accounts where username = '$1'")
BALANCE=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "SELECT balance from accounts where username = '$1'")

if [ $ACCOUNT == 0 ]
then
           echo "OK - $CUSTNAME Musterisinin Bakiyesi=$BALANCE TL , Kredi Limiti=$CREDIT TL dir.|Esik_Seviyesi:$2TL"
           exit 0
else
           echo "KRITIK - $CUSTNAME Musterisinin Bakiyesi Belirlenen Limitin Altina Dusmustur.|Bakiye:$BALANCE, Kredi_Limiti:$CREDIT, Esik_Seviyesi:$2TL"
           exit 2
fi
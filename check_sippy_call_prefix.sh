#!/usr/local/bin/bash

# Bu plugin Monites Monitoring uygulamasının Sippy Softswitch te 
# belirli bir prefixle yapılan premium aramaların database sorgusu ile belirlenebilmesi amacıyla yazılmıştır.
# Her hakkı saklıdır.
# Yazan: Bayram Karagoz [bayram@monites.com]

DATE=$(date -v -3H "+%Y-%m-%d %H:%M:%S+00")
DATE2=$(date -v -$2H -v -3H "+%Y-%m-%d %H:%M:%S+00")


CALL=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "select count(1) from cdrs21 where prefix LIKE '$1%' and prefix NOT LIKE '90%' and connect_time BETWEEN '$DATE2' and '$DATE' and cost > $3 and billed_duration > $4")
DETAIL=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -F "=" -x -c "select cli_in AS Arayan, cld_in AS Aranan, billed_duration AS Sure, prefix AS Prefix, cost AS Maliyet, user_agent AS Client, connect_time AS Zaman from cdrs21 where prefix LIKE '$1%' and prefix NOT LIKE '90%' and connect_time BETWEEN '$DATE2' and '$DATE' and cost > $3 and billed_duration > $4")

if [ $CALL -lt $5 ]
then
           echo "OK - Son $2 Saatte Prefix: $1 Yonunde, $3 TL ve $4 Saniye Uzeri, $CALL Adet Arama Yapildi"
           echo "|$DETAIL"
           exit 0
elif [[ $CALL -ge $5 && $CALL -lt $6 ]]
then
           echo "UYARI - Son $2 Saatte Prefix: $1 Yonunde, $3 TL ve $4 Saniye Uzeri, $CALL Adet Arama Yapildi"
           echo "|$DETAIL"
           exit 1
elif [ $CALL -ge $6 ]
then
           echo "KRITIK - Son $2 Saatte Prefix: $1 Yonunde, $3 TL ve $4 Saniye Uzeri, $CALL Adet Arama Yapildi"
           echo "|$DETAIL"
           exit 2
else
        echo "Bilinmeyen Durum"
        exit 3
fi
#!/usr/local/bin/bash

# Yazan: Bayram Karagoz [bayram@monites.com]

DATE=$(date -v -3H "+%Y-%m-%d %H:%M:%S+00")
DATE2=$(date -v -$2d -v -3H "+%Y-%m-%d %H:%M:%S+00")


CALL=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "select count(1) from cdrs26 where i_account = $1 and billed_duration > 0 and connect_time BETWEEN '$DATE2' and '$DATE'")

CUSTNAME=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "SELECT username from accounts where i_account = $1")


if [ $CALL -gt $3 ]
then
           echo "OK - $CUSTNAME Musterisi Son $2 Gunde $CALL Adet Ucretli Arama Yapmistir.|Esik Seviyesi:$3"
           exit 0
else
                echo "KRITIK - $CUSTNAME Musterisi Son $2 Gunde $CALL Adet Ucretli Arama Yapmistir.|Esik Seviyesi:$3"
                exit 2
fi
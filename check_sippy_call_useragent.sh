#!/usr/local/bin/bash

# Bu plugin Monites Monitoring uygulamasının Sippy Softswitch te 
# belirli bir user agent ile yapılan aramaları database sorgusu ile belirlenebilmesi amacıyla yazılmıştır.
# Her hakkı saklıdır.
# Yazan: Bayram Karagoz [bayram@monites.com]

DATE=$(date -v -3H "+%Y-%m-%d %H:%M:%S+00")
DATE2=$(date -v -$2H -v -3H "+%Y-%m-%d %H:%M:%S+00")


CALL=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -t -c "select count(1) from cdrs21 where user_agent ILIKE '%$1%' and connect_time BETWEEN '$DATE2' and '$DATE'")
DETAIL=$(/usr/local/bin/psql -U sippy -d sippy --no-align --quiet -F "=" -x -c "select cli_in AS Arayan, cld_in AS Aranan, billed_duration AS Sure, cost AS Maliyet, user_agent AS Client from cdrs21 where user_agent ILIKE '%$1%' and connect_time BETWEEN '$DATE2' and '$DATE'")

if [ $CALL == 0 ]
then
           echo "OK - Son $2 Saatte $1 Client ile $CALL Adet Arama Yapildi"
           exit 0
else
           echo "KRITIK - Son $2 Saatte $1 Client ile $CALL Adet Arama Yapildi"
           echo "|$DETAIL"
           exit 2
fi
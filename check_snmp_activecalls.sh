## this plugin created for teles gateway to get con-current call number from pri port
## please use this script as the following line
## ./check_snmp_activecall.sh HOSTIPADDRESS

#!/usr/bin/env bash


CALL=$(snmpwalk -mALL -v1 -cpublic $1 .1.3.6.1.4.1.2170.1.2.1.1.6 |cut -d ' ' -f4|head -n 6|(tr "\012" "+" ; echo "0") | bc)

if [ $CALL -lt $2 ]
then
        echo "OK Active Call =$CALL"
        echo "|Calls =$CALL;;$2"
        exit 0
else
        echo "CRITICAL Active Call =$CALL"
        echo "|Calls =$CALL;;$2"
        exit 2
fi

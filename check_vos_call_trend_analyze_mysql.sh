#!/bin/bash

DATE=`date +%Y%m%d`
DATE2=`date -d '7 days ago' +%Y%m%d`

DATE3=`date +%s`000
DATE5=`date -d '1 hour ago' +%s`000
DATE4=`date -d '7 days ago' +%s`000
DATE6=`date -d '169 hours ago' +%s`000


NOW=$(/usr/bin/mysql --user="root" --password="" -D vos3000 -s -N -e "select count(*) from e_cdr_$DATE where stoptime BETWEEN $DATE5 AND $DATE3 AND holdtime>0")
BEFORE=$(/usr/bin/mysql --user="root" --password="" -D vos3000 -s -N -e "select count(*) from e_cdr_$DATE2  where stoptime BETWEEN $DATE6 AND $DATE4 and holdtime>0")

FARK2=$((NOW-BEFORE))
FARK=${FARK2#-}


if [ $FARK -lt 80 ]
then
    echo "OK- LAST WEEK: $BEFORE , NOW: $NOW, DIFFERENCE: $FARK **TREND ANALYZE RESULT SEEMS NORMAL**"
    exit 0
elif [ $FARK -lt 100 ]
then
    echo "WARNING- LAST WEEK: $BEFORE , NOW: $NOW, DIFFERENCE: $FARK **TREND ANALYZE RESULT SEEMS NOT NORMAL**"
    exit 1
elif [ $FARK -gt 100 ]
then
    echo "CRITICAL- LAST WEEK: $BEFORE , NOW: $NOW, DIFFERENCE: $FARK **ABNORMAL INCREASE OR DICREASE ON CALL DETECTED**"
    exit 2
fi